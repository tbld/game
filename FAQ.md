## FAQ

### What's this "Wolf Inc." thing? Are you selling out?  
Nope, not at all! Tumbled was moved over to `/wolf-inc/tbld` so that it better fits with the projects my friend and I are working on. Wolf Inc. isn't a company, it's a cool name. Nothing else changes.
