# DO's and DON'T's for contributors to `tumbledemerald`:

If you are looking for code style guidelines, see the [style guide](STYLE.md)

DO:

- Keep `tumbledemerald` up-to-date with `@pret`'s decomp. (pret/pokeemerald on GitHub.com)
- Send pull requests
- Send feature requests
- Hang out with us.
- Follow the [style guide](STYLE.md)

DON'T:

- Send a pull request to ask `pret` to merge our code into the main project
- Be uncivilised
- Do anything that isn't in English, French, or German.
- Spam
- Be a phobe (homophobic, etc.) You will be removed from the equation.
