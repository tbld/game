# Got this from a GitHub gist somewhere. If you made this, please reach out so I can add you to the credits!
from glob import iglob
import re

replacement_words = {
    "TINYMUSHROOM": "TinyMushroom",
    "GO-GOGGLES": "Go-Goggles",
    "POISONPOWDER": "PoisonPowder",
    "AND": "and"
}

whole_string_filter = (
    "ABC", "DEF", "GHI", "JKL", "MNO", "PQR", "STU", "VWX", "YZ", 
    "ABCDEFG", "ABCDE", "FGHIJ", "KLMNO", "PQRST", "UVWXY", "Z    ", 
    "ABCDEF .", "GHIJKL ,", "MNOPQRS ", "TUVWXYZ "
)

per_word_filter = (
    "TM", "HM", "KO", "HT", "WT", "HP", "PP", "ZZZ", "SR", 
    "JR", "PC", "RPG", "QTY", "PC", "OT"
)

is_tmhm_pattern = re.compile(r'TM|HM[0-9]{2}')

special_characters = r"éÃãÕõàÀÈÌÒÙàèìòùÛûÁÉÍÓÚáéíóúñÑÇç!#-\-/:-@^_"

lookbehind_braces = r"(?<!\{)"
lookahead_braces = r"(?![\w\s]*[\}])"

seperator_pattern = re.compile(rf"([-']+).*")
uppercase_pattern = re.compile(rf'{lookbehind_braces}([A-Z])([A-Z{special_characters}]+){lookahead_braces}\b')

def process_string(content):
    def repl_func(match):
        orig_str = match.group(0)

        if is_tmhm_pattern.match(orig_str):
            return orig_str

        if seperator_pattern.sub("", orig_str) in per_word_filter:
            return orig_str

        if orig_str in replacement_words:
            return replacement_words[orig_str]
        
        return match.group(1) + match.group(2).lower()

    if content in whole_string_filter:
        return content

    return uppercase_pattern.sub(repl_func, content)

def process_content(content, pattern):
    searches = pattern.finditer(content)

    if not searches:
        return content
    
    for match in searches:
        start, end = match.start(0), match.end(0)
        content = content[:start] + process_string(content[start:end]) + content[end:]

    return content

c_string_pattern = re.compile(r'(?<=_\()(?:(?:\n    |)")([^)]*)(?="\))')
asm_string_pattern = re.compile(r'\.string "(.+)"')

def perform_decap(path, string_pattern, is_glob=False):
    if is_glob:
        for i in iglob(path, recursive=True):
            perform_decap(i, string_pattern, False)

    else:
        with open(path, "r") as source_file:
            content = source_file.read()

        newcontent = process_content(content, string_pattern)

        with open(path, "w") as dest_file:
            dest_file.write(newcontent)

c_files = [
    ("src/**/*.c", True),
    ("src/**/*.h", True),
]

asm_files = [
    ("data/**/*.s", True),
    ("data/**/*.inc", True)
]

if __name__ == "__main__":
    for path, is_glob in c_files:
        perform_decap(path, c_string_pattern, is_glob)

    for path, is_glob in asm_files:
        perform_decap(path, asm_string_pattern, is_glob)
