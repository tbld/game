# Bug Report

## What issue have you found?

<!--- Please provide a detailed explanation of the bug here. -->

## What did you expect to happen instead of this bug?

<!--- What did you expect to happen instead? Please explain in detail. -->

<!--- Please check the following boxes. -->

- [ ] I am using the **latest version** of TumbledEmerald.
- [ ] I am using mGBA, the official CIA package, or a Game Boy Advance flashcart.
