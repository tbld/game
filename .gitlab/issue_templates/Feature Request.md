# Feature Request

## Requested feature: <!--- type what you want added here. -->



## Why do you want this feature?




<!--- Please check the following boxes. -->

- [ ] I have submitted a merge request to implement this feature. <!--- not required, but if this is the case, please indicate so! -->
    - If you have submitted a merge request, please reference it here: <!--- use the !NUMBER format. -->
- [ ] This feature has not been requested in another issue before.
- [ ] I would like to be listed in the credits.

